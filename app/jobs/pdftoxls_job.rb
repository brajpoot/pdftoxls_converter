# frozen_string_literal: true

# PdftoxlsJob
class PdftoxlsJob < ApplicationJob
  def perform(params)
    response = PdftoxlsConverterService.new(params).convert
    file_name = if params[:pdf].original_filename.present?
                  params[:pdf].original_filename.gsub('.pdf', '.xls')
                else
                  'pdftoxls.xls'
                end
    unless response[:success]
      [params[:email], 'brajput@grepruby.io'].each do |email|
        UserMailer.error_to_convert_xls(email, file_name,
                                        response[:error]).deliver_now
      end
      return
    end
    UserMailer.send_xls_file(params[:email], file_name).deliver_now
  end
end
