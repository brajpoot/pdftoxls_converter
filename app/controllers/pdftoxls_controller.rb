# frozen_string_literal: true

# PdftoxlsController
class PdftoxlsController < ApplicationController
  def index; end

  def convert
    PdftoxlsJob.perform_now(params)
    redirect_to root_path, info: 'We are processing your request.'
  end
end
