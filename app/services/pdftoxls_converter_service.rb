# frozen_string_literal: true

require 'pdf/reader'
require 'spreadsheet'
# PdftoxlsConverterService
class PdftoxlsConverterService
  def initialize(params)
    @pdf = params[:pdf]
  end

  def convert
    pdf_data = read_pdf
    generate_xls(pdf_data)
    { error: nil, success: true }
  rescue StandardError => e
    { error: e, success: false }
  end

  def generate_xls(record_group_data)
    book = Spreadsheet::Workbook.new
    sheet1 = book.create_worksheet
    sheet1.row(0).default_format = header_format
    columns_width(sheet1)
    format = Spreadsheet::Format.new pattern_fg_color: :yellow, pattern: 1
    record_group_data.each_with_index do |row, i|
      sheet1.row(i).replace(row)
      sheet1.row(i).default_format = format if row[2].blank?
    end
    book.write('Torrance_new.xls')
  end

  def columns_width(sheet1)
    (0..8).each do |col|
      width = [1, 4, 6].include?(col) ? 50 : 15
      sheet1.column(col).width = width
    end
  end

  def float?(check)
    check.split.map(&:strip).all? do |e|
      Float(e)
    rescue StandardError
      nil
    end
  end

  def header_format
    Spreadsheet::Format.new(
      weight: :bold,
      horizontal_align: :left,
      bottom: :none,
      locked: :none
    )
  end

  def read_pdf
    record_group_data = [['Date', 'Customer', 'Unit', 'Debit Account', 'Debit Desc',
                          'Credit Account', 'Credit Desc', 'Debit Amount', 'Credit Amount']]
    record_group(page_data.flatten)[0..-2].each do |rec|
      record_group_data = record_filtering(rec, record_group_data)
    end
    record_group_data
  end

  def page_data
    pdf = PDF::Reader.new(open(@pdf.path))
    data = []
    pdf.pages.each do |page|
      data << page.text.each_line.to_a.map(&:strip)[8..-2].reject(&:empty?)
    end
    data
  end

  def record_filtering(rec, record_group_data)
    date_time, desc, customer, unit = rec.first.split('  ').reject(&:empty?).map(&:strip)
    date = date_time.strip.split.first
    credit, debit, nil_records_accounts = acc_credit_debit(rec)
    credit, debit = credit_debit_nil(credit, debit, nil_records_accounts)
    filter_record_group_data(credit, debit, record_group_data, date, customer, unit)
  end

  def filter_record_group_data(credit, debit, record_group_data, date, customer, unit)
    max = [credit.length, debit.length].max
    (0..(max - 1)).each do |ind|
      r_data = ['', '', '']
      r_data = [date, customer, unit] if ind.zero?
      record_group_data << (
        r_data +
          [debit.keys[ind].to_s, export_credit_debit(debit, 'desc', ind),
            credit.keys[ind].to_s, export_credit_debit(credit, 'desc', ind),
            export_credit_debit(debit, 'val', ind), export_credit_debit(credit, 'val', ind)])
    end
    record_group_data
  end

  def export_credit_debit(hash_data, val, ind)
    hash_data[hash_data.keys[ind]][val.to_sym].to_s
  rescue
    nil
  end

  def credit_debit_nil(credit, debit, nil_records_accounts)
    unless nil_records_accounts.empty?
      nil_records_accounts.values_at(*%w[a b].each_index.select(&:odd?)).each do |i|
        credit[i] = {}
      end
      nil_records_accounts.values_at(*%w[a b].each_index.select(&:even?)).each do |i|
        debit[i] = {}
      end
    end
    [credit, debit]
  end

  def acc_credit_debit(rec)
    credit = {}
    debit = {}
    nil_records_accounts = []
    rec[1..-2].each do |col|
      credit, debit, nil_records_accounts = credit_debit_by_column(
        col, credit, debit, nil_records_accounts
      )
    end
    [credit, debit, nil_records_accounts]
  end

  def credit_debit_by_column(col, credit, debit, nil_records_accounts)
    desc, val = col.split('  ').map(&:strip).reject(&:empty?)
    coverage_desc = desc[/\$...../]
    acc_no = desc.split.first
    if val.nil?
      nil_records_accounts << acc_no.to_s
    elsif col.length > 110
      credit = acc_credit(credit, acc_no.to_s, val)
      credit[acc_no.to_s][:desc] = coverage_desc.present? ? "Sage #{coverage_desc} Coverage" : ''
    else
      debit = acc_debit(debit, acc_no.to_s, val)
      debit[acc_no.to_s][:desc] = coverage_desc.present? ? "Sage #{coverage_desc} Coverage" : ''
    end
    [credit, debit, nil_records_accounts]
  end

  def acc_credit(credit, acc_no, val)
    credit[acc_no] ||= { val: 0.0, desc: nil }
    credit[acc_no][:val] = (begin
                            credit[acc_no][:val] + Float(val)
                      rescue StandardError
                        nil
                          end)
    credit
  end

  def acc_debit(debit, acc_no, val)
    debit[acc_no] ||= { val: 0.0, desc: nil }
    debit[acc_no][:val] = (begin
                            debit[acc_no][:val] + Float(val)
                     rescue StandardError
                       nil
                          end)
    debit
  end

  def record_group(data)
    record_group = []
    i = 0
    while i < data.length
      record = [data[i]]
      check = data[i + 1]
      i += 2
      record, i = check_float(check, data, record, i)
      record_group << record
    end
    record_group
  end

  def check_float(check, data, record, i)
    while check
      record << check
      break if check.split.length == 2 && float?(check)

      check = data[i]
      i += 1
    end
    [record, i]
  end

  attr_reader :pdf
end
