# frozen_string_literal: true

# UserMailer
class UserMailer < ApplicationMailer
  default from: 'noreplypdftoxls@gmail.com'
  layout 'mailer'
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.send_xls_file.subject
  #
  def send_xls_file(email, file_name)
    @file_name = file_name
    attachments[file_name] = File.read('Torrance_new.xls')
    mail(to: email, subject: 'Please find xls file with attachment.')
  end

  def error_to_convert_xls(email, file_name, error)
    @file_name = file_name.gsub('.xls', '.pdf')
    @error = error
    mail(to: email, subject: 'Could not convert PDF to xls.')
  end
end
