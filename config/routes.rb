Rails.application.routes.draw do
  get 'pdftoxls/index'
  post 'pdftoxls/convert'

  root 'pdftoxls#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
